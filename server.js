var express = require('express');
var app = express();
var path = require('path');

app.use(express.static(path.join(__dirname, '/dist/front-end')));
app.get('*', function (req, res, next) {
    res.sendFile(path.join(__dirname + '/dist/front-end/index.html'));
});

app.listen(400);

