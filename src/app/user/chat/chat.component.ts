import { Component, OnInit } from '@angular/core';
import { MsgService } from 'src/app/shared/service/msg.service';
import { SocketService } from 'src/app/shared/service/socket.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {
  messages = [];
  user;
  msgBody;
  users = [];
  isTyping: boolean = false;
  constructor(
    public socketService: SocketService,
    public msgService: MsgService,
  ) {
    this.user = JSON.parse(localStorage.getItem('user'));
    this.msgBody = {
      senderId: '',
      receiverId: '',
      sender: '',
      receiver: '',
      msg: '',
      time: '',
    }
  }

  ngOnInit() {
    this.socketService.socket.emit('new-user', this.user.username);
    this.runSocket();
  }
  runSocket() {
    this.socketService.socket.on('BE', (data) => {
      console.log('data from BE on socket >>', data);
    });
    this.socketService.socket.on('reply-msg', (data) => {
      this.messages.push(data);
      this.msgBody.receiverId = data.senderId;
    });
    this.socketService.socket.on('reply-msg-own', (data) => {
      this.messages.push(data);
    });
    this.socketService.socket.on('users', (data) => {
      this.users = data;
    });
    this.socketService.socket.on('is-typing', () => {
      this.isTyping = true;
    })
    this.socketService.socket.on('is-typing-stop', () => {
      this.isTyping = false;
    })
  }

  sendMsg() {
    if (!this.msgBody.receiverId) {
      this.msgService.showInfo('Please select a user to continue')
      return;
    }
    if (!this.msgBody.msg) {
      return;
    }
    this.users.forEach((item) => {
      if (item.name == this.user.username) {
        this.msgBody.senderId = item.id;
      }
    })
    this.msgBody.sender = this.user.username;
    this.msgBody.time = new Date();

    this.socketService.socket.emit('new-msg', this.msgBody);
    this.msgBody.msg = '';
  }

  selectUser(user) {
    this.msgBody.receiverId = user.id;
  }

  focused(val) {
    // console.log('value >>', val);
    if (val) {
      this.socketService.socket.emit('typing', this.msgBody);
    } else {
      this.socketService.socket.emit('typing-stop', this.msgBody);
    }
  }
}
