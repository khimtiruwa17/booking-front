import { NgModule } from "@angular/core";
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ProfileComponent } from './profile/profile.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { ChatComponent } from './chat/chat.component';

const userRoutes: Routes = [
    {
        path: 'dashboard',
        component: DashboardComponent
    },
    {
        path: 'profile',
        component: ProfileComponent,
    },
    {
        path: 'change-password',
        component: ChangePasswordComponent
    },
    {
        path: 'chat',
        component: ChatComponent
    }
]
@NgModule({
    imports: [
        RouterModule.forChild(userRoutes),
    ],
    exports: [RouterModule]
})
export class UserRoutingModule {

}