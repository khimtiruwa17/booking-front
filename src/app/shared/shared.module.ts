import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { MsgService } from './service/msg.service';
import { RouterModule } from '@angular/router';
import { SocketService } from './service/socket.service';
import { AuthGuard } from './service/auth-guard.service';
import { NotFoundComponent } from './not-found/not-found.component';



@NgModule({
  declarations: [PageNotFoundComponent, NotFoundComponent],
  imports: [
    CommonModule,
    RouterModule
  ],
  providers: [
    MsgService,
    SocketService,
    AuthGuard
  ],
})
export class SharedModule { }
