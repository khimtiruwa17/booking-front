import { Injectable } from "@angular/core";
import { ToastrService } from 'ngx-toastr';
import { isArray } from 'util';

Injectable()

export class MsgService {

    constructor(public toastr: ToastrService) {

    }

    showSuccess(msg: string) {
        this.toastr.success(msg);
    }

    showInfo(msg: string) {
        this.toastr.info(msg);
    }
    showWarning(msg: string) {
        this.toastr.warning(msg);
    }
    showError(err: any) {
        debugger;
        //check what is the actual error and show appropirate error message 
        // this.toastr.error('show some error');
        if (err.error) {
            var errorMsg = err.error.message;
            if (isArray(errorMsg)) {
                this.error(errorMsg[0].msg)
            } else {
                this.error(errorMsg);
            }
        }
    }


    error(msg: string) {
        this.toastr.info(msg);
    }
}
