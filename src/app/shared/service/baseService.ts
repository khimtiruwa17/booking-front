import { environment } from 'src/environments/environment';
import { HttpHeaders } from '@angular/common/http';


export class BaseService {
    url;
    constructor() {
        this.url = environment.BaseUrl;
    }

    setUrl(url: string) {
        this.url += url + '/';
    }


    getHeaders() {
        return {
            headers: new HttpHeaders({
                'Content-Type': 'application/json'
            })
        }
    }
    getHeaderswithToken() {
        // retunr hea
        return {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'x-access-token': localStorage.getItem('token')
            })
        }
    }


}