import { NgModule } from "@angular/core";
import { RouterModule, Routes } from '@angular/router';
import { BookRoomComponent } from './book-room/book-room.component';
import { CancelBookingComponent } from './cancel-booking/cancel-booking.component';
import { UpdateBookingComponent } from './update-booking/update-booking.component';
import { ListBookingComponent } from './list-booking/list-booking.component';


const bookingRoutes: Routes = [
    {
        path: 'book/:id',
        component: BookRoomComponent
    },
    {
        path: 'list',
        component: ListBookingComponent
    },
    {
        path: 'cancel',
        component: CancelBookingComponent
    },
    {
        path: 'update',
        component: UpdateBookingComponent
    },
]
@NgModule({
    imports: [
        RouterModule.forChild(bookingRoutes),
    ],
    exports: [
        RouterModule
    ]
})
export class BookingRoutingModule {

}