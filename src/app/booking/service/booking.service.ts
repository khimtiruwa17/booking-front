import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
// import {user} from '../../shared/'
// import { User } from './../models/user.models';
import { Booking } from '../model/booking.model'
import { BaseService } from '../../shared/service/baseService';
@Injectable()
export class BookingService extends BaseService {
    //extends {//BaseService {
    // token;
    constructor(
        public http: HttpClient
    ) {
        super();
        this.setUrl('booking');
    }
    book(data: Booking) {
        console.log('url is', this.url)
        console.log('bookingService data is', data)
        return this.http.post(`${this.url}`, data, this.getHeaders())
    }
    getBookingList() {
        // return this.http.get(this.url, this.getHeaderswithToken());
        return this.http.get(this.url);
    }
    confirmbooking(id: string, data: Booking) {
        // return this.http.put(this.url, data: Booking, token);
        return this.http.put(this.url + id, data, this.getHeaderswithToken());
    }
    rejectBooking(id: string, data: any) {
        return this.http.put(this.url + id, data, this.getHeaderswithToken());
    }
    update(id: string, data: Booking) {
        return this.http.put(this.url + id, data);
    }
}