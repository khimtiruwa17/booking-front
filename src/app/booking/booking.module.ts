import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BookRoomComponent } from './book-room/book-room.component';
import { CancelBookingComponent } from './cancel-booking/cancel-booking.component';
import { UpdateBookingComponent } from './update-booking/update-booking.component';
import { BookingRoutingModule } from './booking.routing';
import { SharedModule } from '../shared/shared.module';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ListBookingComponent } from './list-booking/list-booking.component';
import { BookingService } from './service/booking.service';

@NgModule({
  declarations: [
    BookRoomComponent,
    CancelBookingComponent,
    UpdateBookingComponent,
    ListBookingComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    BookingRoutingModule,
    SharedModule,

  ],
  providers: [
    BookingService
  ]
})
export class BookingModule { }
