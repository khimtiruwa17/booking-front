// import { Component, OnInit } from '@angular/core';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MsgService } from 'src/app/shared/service/msg.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Booking } from '../model/booking.model';
import { BookingService } from '../service/booking.service';
import { ProductService } from 'src/app/product/service/product.serivce';
@Component({
  selector: 'app-book-room',
  templateUrl: './book-room.component.html',
  styleUrls: ['./book-room.component.css'],
  // template: './formlayout.html',
  // styleUrls: ['./form.css']
})
export class BookRoomComponent implements OnInit {
  booking;
  submitting: boolean = false;
  public message: string;
  roomId: any
  room: any
  // room = []
  @Input() childMessage: string;
  constructor(
    public router: Router,
    public activeRoute: ActivatedRoute,
    public msgService: MsgService,
    public bookingService: BookingService,
    public productService: ProductService,

  ) {
    this.booking = new Booking({});
    this.roomId = this.activeRoute.snapshot.params['id'];

  }

  ngOnInit() {
    // this.booking = new Booking({})
    // console.log(`message from list product is `, this.childMessage)
    this.getRoom();

  }
  getRoom() {
    this.productService.getById(this.roomId).subscribe(
      (data) => {
        this.room = data;
        console.log(`room dat to book are `, this.room)
      }, error => {
        // this.msgService.showError(error);
        console.log(`room dat to book are `, error)
      }
    )
  }

  book() {
    // alert("hello");
    this.submitting = true;
    console.log(`booking data are`, this.booking);
    this.bookingService.book(this.booking).subscribe(
      // console.log(data),
      (data: any) => {
        this.submitting = false;
        this.msgService.showSuccess('Booked successfully');
        // this.router.navigate(['/booking/list']);
        // this.router.navigate(['/user/myplaces']);
      }, error => {
        this.submitting = false;
        // console.log(`product data are`, error);
        this.msgService.showError(error);
      }
    )
  }

}
