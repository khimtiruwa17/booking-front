export class Booking {
    _id: string;
    firstname: string;
    lastname: string;
    contactNumber: string;
    email: string;
    city: string;
    status: string;
    // user: string;
    constructor(obj: any) {
        for (let key in obj) {
            this[key] = obj[key] || '';
        }
    }
}