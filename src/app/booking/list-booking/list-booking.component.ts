import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MsgService } from 'src/app/shared/service/msg.service';
import { BookingService } from '../service/booking.service';
import * as moment from 'moment';
import { Booking } from "../model/booking.model"
@Component({
  selector: 'app-list-booking',
  templateUrl: './list-booking.component.html',
  styleUrls: ['./list-booking.component.css']
})
export class ListBookingComponent implements OnInit {
  bookings = [];
  bookingId; booking;
  loading: boolean = false;
  ago;
  constructor(
    public bookingService: BookingService,
    public router: Router,
    public msgService: MsgService
  ) {
    this.ago = moment;
    this.booking = new Booking({})
  }

  ngOnInit() {
    this.loading = true;
    this.getbookinglist();
  }
  getbookinglist() {
    this.bookingService.getBookingList().subscribe(
      (data: any) => {
        this.loading = false;
        this.bookings = data;
        console.log('booking data are  :', this.bookings);
      },
      error => {
        this.loading = false;
        this.msgService.showError(error);
      }
    )
  }
  confirmbooking(id, i) {
    // remove(id, i) {
    console.log(`booking id is db`, id);
    // this.bookings = [
    //   status = 'confirm'
    // ]
    this.booking.status = 'confirm';
    console.log(`booking confirm data are`, this.booking)
    this.bookingService.confirmbooking(id, this.booking).subscribe(
      (data) => {
        // this.submitting = false;
        this.router.navigate(['/booking/list']);
        this.msgService.showSuccess('confirmed');
        this.getbookinglist();
      }, error => {
        // this.submitting = false;
        this.msgService.showError(error);
      }
    )
  }
  rejectBooking(id) {
    console.log(`booking id is db`, id);
    // this.bookings = [
    //   status = 'reject'
    // ]
    this.booking.status = 'reject';
    console.log(`booking  reject data are`, this.booking)
    this.bookingService.rejectBooking(id, this.booking).subscribe(
      (data) => {
        this.msgService.showSuccess('rejected');
        this.router.navigate(['/booking/list']);
        this.getbookinglist();
      },
      error => {
        this.msgService.showError(error);
      }
    )
  }

  // showSearchMenu() {
  //   this.showAgain.emit(true);
  // }

}
