export class User {
    name: string;
    username: string;
    password: string;
    email: string;
    phoneNumber: string;
    address: string;
    gender: string;
    _id: string | number;
    createdAt: string;
    updatedAt: string;
    confirmPassword: string;
    passwordResetToken: string;


    constructor(obj: any) {
        // this.name = obj.name || '';
        // this.username = obj.username || '';
        // this.password = obj.password || '';
        // this.email = obj.email || '';
        // this.phoneNumber = obj.phoneNumber || '';
        // this.address = obj.address || '';
        // this.gender = obj.gender || '';

        for (let key in obj) {
            this[key] = obj[key] || '';
        }
    }
}

// interface a {
//     name: string
// }