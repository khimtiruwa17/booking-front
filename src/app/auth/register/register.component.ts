import { Component, OnInit } from '@angular/core';
import { AuthService } from '../service/auth.service';
import { Router } from '@angular/router';
import { MsgService } from 'src/app/shared/service/msg.service';
import { User } from './../models/user.models';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  title: string = 'Registration';
  user; filesToUpload = [];
  submitting: boolean = false;
  constructor(
    public authService: AuthService,
    public router: Router,
    public msgService: MsgService,
  ) {
    this.user = new User({});
    // console.log(this.user);
  }
  ngOnInit() {
    // console.log('initialized ', this.user);
  }
  fileChanged(event) {
    this.filesToUpload = event.target.files;
  }
  submit() {
    this.submitting = true;
    this.authService.register(this.user).subscribe(
      (data) => {
        this.msgService.showSuccess('Sucessfully Registerd' + ' check your email for verification');
        this.router.navigate(['/auth/login'])
      },
      (error) => {
        console.log('error in regestring');
        this.msgService.showError(error);
      }
    )
  }

  // upload() {
  //   this.submitting = true;
  //   this.authService.upload(this.user, this.filesToUpload, "POST").subscribe(
  //     (data: any) => {
  //       this.submitting = false;
  //       console.log('my user reg respose data is: :', data);
  //       this.msgService.showSuccess('Sucessfully Registerd' + ' check your email for verification');
  //       this.router.navigate(['/auth/login']);
  //     }, error => {
  //       this.submitting = false;
  //       console.log('error is :', error);
  //       // console.log('error in regestring');
  //       this.msgService.showError(error);
  //     }
  //   )
  // }
}
