import { Component, OnInit } from '@angular/core';
import { AuthService } from '../service/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from './../models/user.models'
import { MsgService } from 'src/app/shared/service/msg.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  user; returnUrl: string;
  submitting: boolean = false;
  constructor(
    public router: Router,
    // private route: ActivatedRoute,
    public authService: AuthService,
    private msgService: MsgService
  ) {
    this.user = new User({});
    console.log('localstoreage user', this.authService.getUser());
  }

  ngOnInit() {
    document.body.className = "selector";
    // this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    // if (this.authService.isLoggedIn()) {
    // this.router.navigate(['/user/dashboard']);
    // }

  }

  ngOnDestroy() {
    document.body.className = "";
  }
  login() {

    this.submitting = true;
    this.authService.login(this.user)
      .subscribe(
        (data: any) => {
          this.submitting = false;
          // console.log('success in observable lgoin', data);
          localStorage.setItem('token', data.token);
          localStorage.setItem('user', JSON.stringify(data.user));
          this.msgService.showSuccess('Welcome ' + data.user.username);
          this.router.navigate(['/user/dashboard']);
          // this.router.navigate(['/product/list']);


        },
        error => {
          this.submitting = false;
          this.msgService.showError(error);
        }
      )
  }
}
