import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
// import {user} from '../../shared/'
import { User } from './../models/user.models';
import { BaseService } from '../../shared/service/baseService';
@Injectable()
export class AuthService extends BaseService {
    //extends {//BaseService {

    constructor(
        public http: HttpClient
    ) {
        super();
        this.setUrl('auth');
    }

    login(data: User) {
        console.log('url is', this.url)
        return this.http.post(`${this.url}login`, data, this.getHeaders())

    }

    register(data: User) {
        return this.http.post(`${this.url}register`, data, this.getHeaders())
    }

    forgotPassword(data: User) {
        return this.http.post(`${this.url}forgot-password`, data, this.getHeaders());
    }

    resetPassword(data: User) {
        return this.http.post(`${this.url}reset-password/${data.passwordResetToken}`, data, this.getHeaders());
    }

    getUser() {
        return JSON.parse(localStorage.getItem('user'));
    }

    isLoggedIn() {
        if (localStorage.getItem('token')) {
            return true;
        } else {
            return false;
        }
    }

    isAuthenticated() {

    }
}