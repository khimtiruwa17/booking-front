import { Component, OnInit } from '@angular/core';
import { AuthService } from '../service/auth.service';
import { MsgService } from 'src/app/shared/service/msg.service';
import { Router } from '@angular/router';
import { User } from './../models/user.models';
@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {

  submitting: boolean = false;
  user;
  constructor(
    public authService: AuthService,
    public msgService: MsgService,
    public router: Router
  ) {
    this.user = new User({});
  }
  ngOnInit() {
  }
  submit() {
    this.submitting = true;
    this.authService.forgotPassword(this.user).subscribe(
      data => {
        this.submitting = false;
        this.msgService.showSuccess('Reset password link sent to email please check your email');
        this.router.navigate(['/auth/login']);
        console.log(`dataa arreee`, data)
      },
      error => {
        this.submitting = false;
        this.msgService.showError(error);
      }
    )
  }
}