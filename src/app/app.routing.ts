import { Routes, RouterModule } from '@angular/router';

import { NgModule } from '@angular/core';
import { AuthGuard } from './shared/service/auth-guard.service';
import { PageNotFoundComponent } from './shared/page-not-found/page-not-found.component';

const routeConfig: Routes = [
    {
        path: '',
        redirectTo: '/auth/login',
        pathMatch: 'full'
    },
    {
        path: 'auth',
        loadChildren: './auth/auth.module#AuthModule',

    },
    {
        path: 'user',
        loadChildren: './user/user.module#UserModule',
        canActivate: [AuthGuard]
    },
    {
        path: 'product',
        loadChildren: './product/product.module#ProductModule',
        // canActivate: [AuthGuard]
    },
    {
        path: 'booking',
        loadChildren: './booking/booking.module#BookingModule',
        // canActivate: [AuthGuard]
    },
    {
        path: '**',
        component: PageNotFoundComponent

    }
]

@NgModule({
    imports: [RouterModule.forRoot(routeConfig)],
    exports: [RouterModule]

})
export class AppRoutingModule {

}