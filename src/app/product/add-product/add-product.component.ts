import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';

import { Ng2ImgMaxService } from 'ng2-img-max';
import { DomSanitizer } from '@angular/platform-browser';

import { Product } from './../models/product.model';
import { ProductService } from '../service/product.serivce';
import { MsgService } from 'src/app/shared/service/msg.service';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css']
})
export class AddProductComponent implements OnInit {
  product;
  submitting: boolean = false;
  filesToUpload = []; uploadedImage;
  imagePath; imagePreview;
  imgURL: any;
  public message: string;
  constructor(
    public msgService: MsgService,
    public router: Router,
    public productService: ProductService,
    // private ng2ImgMax: Ng2ImgMaxService,
    public sanitizer: DomSanitizer
  ) {
    this.product = new Product({})
  }
  ngOnInit() {

  }

  fileChanged(event) {
    this.filesToUpload = event.target.files;
    var files = this.filesToUpload;
    if (files.length === 0)
      return;
    var mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      this.message = "Only images are supported.";
      return;
    }
    var reader = new FileReader();
    this.imagePath = files;
    reader.readAsDataURL(files[0]);
    reader.onload = (_event) => {
      this.imgURL = reader.result;
    }
  }
  /**
   * working for adding product
   */
  upload() {
    // alert("hello");
    this.submitting = true;
    console.log(`product data are`, this.product);
    this.productService.upload(this.product, this.filesToUpload, "POST").subscribe(
      // console.log(data),
      (data: any) => {
        this.submitting = false;
        this.msgService.showSuccess('Product Added successfully');
        this.router.navigate(['/product/list']);
        // this.router.navigate(['/user/myplaces']);
      }, error => {
        this.submitting = false;
        console.log(`product data are`, error);
        this.msgService.showError(error);
        // this.msgService.showError(error);
      }
    )
  }
}
