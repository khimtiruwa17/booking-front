import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MsgService } from 'src/app/shared/service/msg.service';
import { Router } from '@angular/router';
import { ProductService } from '../service/product.serivce';
import { Product } from './../models/product.model';
import { environment } from './../../../environments/environment';
import { AuthService } from 'src/app/auth/service/auth.service';
import * as moment from 'moment';
@Component({
  selector: 'app-list-product',
  // templateUrl: './list-product.component.html',
  templateUrl: './card.html',
  styleUrls: ['./list-product.component.css']
})
export class ListProductComponent implements OnInit {
  products: Array<Product>;
  loading: boolean = false;
  imgUrl; ago;
  parentMessage = "message from parent"
  constructor(
    public msgService: MsgService,
    public router: Router,
    public productService: ProductService,
    public authService: AuthService

  ) {
    this.imgUrl = environment.ImgUrl + 'product/';
    this.ago = moment;
  }
  ngOnInit() {
    document.body.className = "khim";
    console.log('image url is :', this.imgUrl);
    this.loading = true;
    this.productService.list().subscribe(
      (data: any) => {
        this.loading = false;
        this.products = data;
      },
      error => {
        this.loading = false;
        this.msgService.showError(error);
      }
    )
  }

  ngOnDestroy() {
    document.body.className = "";
  }
  /**
   * 
   * @param productId 
   */
  edit(productId) {
    var updateurl = '/product/edit/' + productId;
    this.router.navigate([updateurl]);
  }
  /**
   * 
   * @param productId 
   */
  book(productId) {
    var booking = '/product/book/' + productId;
    this.router.navigate([booking]);
  }
  /**
   * @param id product id
   * @param index vale from table index
   */
  remove(id, index) {
    let confirmation = confirm('Are you sure to remove?');
    if (confirmation) {
      this.productService.remove(id).subscribe(
        (data) => {
          this.msgService.showInfo("Product deleted!");
          this.products.splice(index, 1);
        },
        error => {
          this.msgService.showError(error);
        }
      )
    }
  }
}
