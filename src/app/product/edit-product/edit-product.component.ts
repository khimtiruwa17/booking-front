import { Component, OnInit } from '@angular/core';
import { MsgService } from 'src/app/shared/service/msg.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ProductService } from '../service/product.serivce';
import { environment } from './../../../environments/environment';
@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.css']
})
export class EditProductComponent implements OnInit {
  productId;
  filesToUplaod = [];
  imgUrl;
  loading: boolean = false;
  product;
  submitting: boolean = false;
  constructor(
    public msgService: MsgService,
    public router: Router,
    public activeRoute: ActivatedRoute,
    public productService: ProductService
  ) {
    this.productId = this.activeRoute.snapshot.params['id'];
    // this.productId = this.activeRoute.snapshot.params['id'];
    this.imgUrl = environment.ImgUrl + 'product/';
  }

  ngOnInit() {
    this.loading = true;
    this.productService.getById(this.productId).subscribe(
      (data) => {
        this.loading = false;
        this.product = data;
        console.log('edit product data are', this.product)
        this.product.tags = this.product.tags.join(',');
      }, error => {
        this.loading = false;
        this.msgService.showError(error);
      }
    )
  }
  update() {
    this.submitting = true;
    this.productService.update(this.productId, this.product).subscribe(
      (data) => {
        this.submitting = false;
        this.router.navigate(['/product/list']);
        this.msgService.showSuccess('Product updated successfully');
      }, error => {
        this.submitting = false;
        this.msgService.showError(error);
      }
    )
  }
  fileChanged(ev) {
    this.filesToUplaod = ev.target.files;
  }
  upload() {
    this.submitting = true;
    this.productService.upload(this.product, this.filesToUplaod, "PUT").subscribe(
      (data) => {
        this.submitting = false;
        this.router.navigate(['/product/list']);
        this.msgService.showSuccess('Product updated successfully');
      }, error => {
        this.submitting = false;
        this.msgService.showError(error);
      }
    )
  }

}
