export class Product {
    _id: string;
    name: string;
    description: string;
    price: string;
    category: string;
    tags: string
    for: string;
    address: string;
    status: string;
    image: string;

    user: string;
    constructor(obj: any) {
        for (let key in obj) {
            this[key] = obj[key] || '';
        }
    }
}