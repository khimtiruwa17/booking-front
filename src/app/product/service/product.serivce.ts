import { Injectable } from "@angular/core";
// import { BaseService } from 'src/app/shared/services/base.service';
import { BaseService } from '../../shared/service/baseService';
import { HttpClient } from '@angular/common/http';
import { Product } from '../models/product.model';
import { Observable } from 'rxjs';

Injectable()
export class ProductService extends BaseService {
    token;
    constructor(public http: HttpClient) {
        super();
        this.setUrl('product');
        this.token = localStorage.getItem('token');
    }

    add(data: Product) {
        return this.http.post(this.url, data, this.getHeaderswithToken());
    }

    list() {
        // return this.http.get(this.url, this.getHeaderswithToken());
        return this.http.get(this.url);
    }

    getById(id: string) {
        // return this.http.get(this.url + id)
        return this.http.get(this.url + id, this.getHeaderswithToken())
    }
    update(id: string, data: Product) {
        return this.http.put(this.url + id, data, this.getHeaderswithToken());
    }
    remove(id: string) {
        return this.http.delete(this.url + id, this.getHeaderswithToken());
    }

    search(data: Product) {
        console.log('data is ', data);
        return this.http.post(this.url + 'search', data, this.getHeaderswithToken())

    }
    upload(data: Product, files: any, method: any) {
        // console.log(`data are `, data)
        // console.log(`url are`, this.url)
        return Observable.create((observer) => {
            const formData = new FormData;
            const xhr = new XMLHttpRequest;

            if (files) {
                formData.append('img', files[0], files[0].name);
            }
            for (let key in data) {
                formData.append(key, data[key]);
            }

            xhr.onreadystatechange = () => {
                if (xhr.readyState == 4) {
                    if (xhr.status == 200) {
                        // console.log('file upload success');
                        observer.next(xhr.response);
                    } else {
                        // console.log('file upload failed');
                        observer.error(xhr.response);
                    }
                }
            }
            let url;

            if (method == "POST") {
                url = `${this.url}?token=${this.token}`
            } else {
                url = `${this.url}${data._id}?token=${this.token}`
            }
            console.log(`url are`, url)

            xhr.open(method, url, true);
            xhr.send(formData);
        })


    }

}