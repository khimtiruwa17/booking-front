import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddProductComponent } from './add-product/add-product.component';
import { EditProductComponent } from './edit-product/edit-product.component';
import { ListProductComponent } from './list-product/list-product.component';
import { AuthGuard } from '../shared/service/auth-guard.service';

const productRoutes: Routes = [
    {
        path: 'add',
        component: AddProductComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'list',
        component: ListProductComponent
    },
    {
        path: 'edit/:id',
        component: EditProductComponent,
        canActivate: [AuthGuard]
    },

]
@NgModule({
    imports: [
        RouterModule.forChild(productRoutes)
    ],
    exports: [RouterModule]
})

export class ProductRoutingModule {

}