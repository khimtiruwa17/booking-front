import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddProductComponent } from './add-product/add-product.component';
import { EditProductComponent } from './edit-product/edit-product.component';
import { ProductRoutingModule } from './product.routing';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ProductService } from './service/product.serivce';
import { ListProductComponent } from './list-product/list-product.component';
import { SharedModule } from '../shared/shared.module';
// import { BookingModule } from '../booking/booking.module';

@NgModule({
  declarations: [
    AddProductComponent,
    ListProductComponent,
    EditProductComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    SharedModule,
    // BookingModule,
    ProductRoutingModule,
  ],
  providers: [
    ProductService
  ]
})
export class ProductModule { }
